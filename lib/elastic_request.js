var _ = require( "underscore" ),
    elasticsearch = require( "elasticsearch" ),
    MapGenerator = require( "./map_generator" ),
    ElasticRequest = { esClient: null };

ElasticRequest.createClient = function( ) {
  if( ElasticRequest.esClient === null ) {
    ElasticRequest.esClient = new elasticsearch.Client({
      host: global.config.elasticsearch.host,
      log: false
    });
  }
};

ElasticRequest.count = function (req, callback) {
    // only style for density needs adjusting based on entity count
    if (req.params.kpi !== 'density') return callback(null, req);

    var query = _.extend({}, req.elastic_query, {aggregations: undefined}),
        searchOpts = {
            preference: global.config.elasticsearch.preference || "_local",
            index: req.elastic_index || global.config.elasticsearch.searchIndex,
            type: req.elastic_type,
            body: query
        };

    ElasticRequest.createClient();
    ElasticRequest.esClient.search(searchOpts, function (err, res) {
        if (err) {
            return callback(err, null);
        }

        req.total = res.hits.total;
        callback(null, req);
    });
};

ElasticRequest.search = function (req, callback) {
    var query = _.extend({}, req.elastic_query),
        searchOpts = {
            preference: global.config.elasticsearch.preference || "_local",
            index: req.elastic_index || global.config.elasticsearch.searchIndex,
            type: req.elastic_type,
            body: query
        },
        shouldScroll = !query.aggregations,
        result;

    if (shouldScroll) {
        _.extend(searchOpts, {
            searchType: ( query.size === 0 ? "count" : null ),
            scroll: "30s"
        });
    }

    ElasticRequest.createClient();
    ElasticRequest.esClient.search(searchOpts, function more(error, response) {
        if (error) {
            return callback(error, null);
        }
        
        if (shouldScroll) {
            if (result) {
                result.took += response.took;
                result.hits.hits = result.hits.hits.concat(response.hits.hits);
            } else {
                result = response;
            }

            if (response.hits.total !== result.hits.hits.length) {
                ElasticRequest.esClient.scroll({
                    scrollId: response._scroll_id,
                    scroll: "30s"
                }, more);
            } else {
                ElasticRequest.esClient.clearScroll(response._scroll_id);
                callback(error, result);
            }
        } else {
            callback(null, response);
        }
    });
};

ElasticRequest.expandBoxForSmoothEdges = function( qbbox ) {
  var height = Math.abs( qbbox[2] - qbbox[0] );
  var width = Math.abs( qbbox[3] - qbbox[1] );
  var factor = 0.07;
  qbbox[0] = qbbox[0] - ( height * factor );
  qbbox[2] = qbbox[2] + ( height * factor );
  qbbox[1] = qbbox[1] - ( width * factor );
  qbbox[3] = qbbox[3] + ( width * factor );
  if( qbbox[0] < -180 ) { qbbox[0] = -180; }
  if( qbbox[1] < -90 ) { qbbox[1] = -90; }
  if( qbbox[2] > 180 ) { qbbox[2] = 180; }
  if( qbbox[3] > 90 ) { qbbox[3] = 90; }
  return qbbox;
};

ElasticRequest.boundingBoxFilter = function( qbbox, smoothing ) {
  if( smoothing !== false) {
    qbbox = ElasticRequest.expandBoxForSmoothEdges( qbbox );
  }
  if( qbbox[2] < qbbox[0] ) {
    // the envelope crosses the dateline. Unfortunately, elasticsearch
    // doesn't handle this well and we need to split the envelope at
    // the dateline and do an OR query
    var left = _.clone( qbbox );
    var right =_.clone( qbbox );
    left[2] = 180;
    right[0] = -180;
    return { or: [
      ElasticRequest.boundingBoxFilter( left, false ),
      ElasticRequest.boundingBoxFilter( right, false ) ] };
  }

  var field = global.config.elasticsearch.geoPointField;
  var boundingBox = { };
  boundingBox[field] = {
    bottom_left: [ qbbox[0], qbbox[1] ],
    top_right: [ qbbox[2], qbbox[3] ]
  };
  boundingBox.type = "indexed";
  return { "geo_bounding_box" : boundingBox };
};

ElasticRequest.geohashPrecision = function( zoom ) {
  var precision = 3;
  if( zoom >= 2 ) { precision = 4; }
  if( zoom >= 4 ) { precision = 5; }
  if( zoom >= 6 ) { precision = 6; }
  if( zoom >= 9 ) { precision = 7; }
  if( zoom >= 10 ) { precision = 8; }
  if( zoom >= 13 ) { precision = 9; }
  if( zoom >= 15 ) { precision = 10; }
  if( zoom >= 16 ) { precision = 12; }
  return precision;
};

ElasticRequest.defaultMapFields = function( ) {
  return [ "id", global.config.elasticsearch.geoPointField ];
};

ElasticRequest.defaultMapQuery = function( ) {
  return { match_all: { } };
};

ElasticRequest.applyBoundingBoxFilter = function (req) {
    if (req.params.dataType === "geojson" ||
        req.params.dataType === "postgis") {
        return;
    }

    MapGenerator.createMercator();

    req.bbox = MapGenerator.merc.convert(MapGenerator.bboxFromParams(req));
    var bboxFilter = ElasticRequest.boundingBoxFilter(req.bbox);

    if (!req.elastic_query.query.bool.filter) {
        req.elastic_query.query.bool.filter = [];
    }

    // there is an existing array of filters
    if (_.isArray(req.elastic_query.query.bool.filter)) {
        req.elastic_query.query.bool.filter.push(bboxFilter);
    }
};

ElasticRequest.geohashAggregation = function( req ) {
  return {
    zoom1: {
      geohash_grid: {
        field: global.config.elasticsearch.geoPointField,
        size: 20000,
        precision: ElasticRequest.geohashPrecision( req.params.zoom )
/*      },
      aggs: {
        geohash: {
          top_hits: {
            sort: { id: { order: "desc" } },
            _source: (( req.query && req.query.source ) ?
              req.query.source : false ),
            fielddata_fields: req.elastic_query.fields,
            size: 1
          }
        }*/
      }
    }
  };
};

module.exports = ElasticRequest;
