var Styles = {};

// taken from https://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors
function shadeColor2(color, percent) {
    var f = parseInt(color.slice(1), 16),
        r = f >> 16,
        g = f >> 8 & 0x00FF,
        b = f & 0x0000FF,
        t = percent < 0 ? 0 : 255,
        p = percent < 0 ? percent * -1 : percent;

    return "#" + (0x1000000 + (Math.round((t - r) * p) + r) * 0x10000 + (Math.round((t - g) * p) + g) * 0x100 + (Math.round((t - b) * p) + b)).toString(16).slice(1);
}

Styles.points = function (req) {
    var defaultColors = "#97b1df, #4a6ca7, #32456f",
        defaultMobchColors = "#cde7ea, #99cad5, #66b1bf, #3295a9, #017c93",
        total = req.total,
        color = decodeURIComponent(req.query.color);

    var colors;
    if (/^#?([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/.test(color)) {
        if (!color.startsWith('#')) color = `#${color}`;
        colors = `${shadeColor2(color, 0.6)}, ${color}`;
    } else if (req.params.tenant == 'mobch') {
        colors = defaultMobchColors;
    } else {
        colors = defaultColors;
    }

    return `\
        <Style name='points' filter-mode='first' image-filters='colorize-alpha(${colors})'>\
          <Rule>\
            <MaxScaleDenominator>${total > 10000 ? 25000 : Number.MAX_SAFE_INTEGER}</MaxScaleDenominator>\
            <MarkersSymbolizer width='5' opacity='${total > 100 ? 0.6 : 0.9}' allow-overlap='true' ignore-placement='true' />\
          </Rule>\
          <Rule>\
            <MaxScaleDenominator>200000</MaxScaleDenominator>\
            <MinScaleDenominator>25000</MinScaleDenominator>\
            <MarkersSymbolizer width='5' opacity='0.1' allow-overlap='true' ignore-placement='true' />\
          </Rule>\
          <Rule>\
            <MinScaleDenominator>200000</MinScaleDenominator>\
            <MarkersSymbolizer width='5' opacity='0.05' allow-overlap='true' ignore-placement='true' />\
          </Rule>\
        </Style>`;
};

Styles.attractiveness_low = function (field) {
    return `\
      <Style name='attractiveness_low' filter-mode='first' image-filters='colorize-alpha(#e1f3e7, #b5e1c3)'>\
        <Rule>\
          <Filter>[${field}] = 2</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.6' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
        <Rule>\
          <Filter>[${field}] = 1</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.4' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
      </Style>`;
};

Styles.attractiveness_medium = function (field) {
    return `\
      <Style name='attractiveness_medium' filter-mode='first' image-filters='colorize-alpha(#83cd9b, #5cb278)'>\
        <Rule>\
          <Filter>[${field}] = 4</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.6' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
        <Rule>\
          <Filter>[${field}] = 3</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.4' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
      </Style>`;
};

Styles.attractiveness_high = function (field) {
    return `\
      <Style name='attractiveness_high' filter-mode='first' image-filters='colorize-alpha(#008a2e, #006f25)'>\
        <Rule>\
          <Filter>[${field}] = 6</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.6' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
        <Rule>\
          <Filter>[${field}] = 5</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.4' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
      </Style>`;
};

Styles.location_low = function (field) {
    return `\
      <Style name='location_low' filter-mode='first' image-filters='colorize-alpha(#b6a1ab, #9a7988)'>\
        <Rule>\
          <Filter>[${field}] = 2</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.6' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
        <Rule>\
          <Filter>[${field}] = 1</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.4' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
      </Style>`;
};

Styles.location_medium = function (field) {
    return `\
      <Style name='location_medium' filter-mode='first' image-filters='colorize-alpha(#8e6477, #7e4a62)'>\
        <Rule>\
          <Filter>[${field}] = 3</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.6' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
      </Style>`;
};

Styles.location_high = function (field) {
    return `\
      <Style name='location_high' filter-mode='first' image-filters='colorize-alpha(#6d2c4a, #55042a)'>\
        <Rule>\
          <Filter>[${field}] = 4</Filter>\
          <MarkersSymbolizer file='marker.png' width='24' opacity='0.6' allow-overlap='true' ignore-placement='true' />\
        </Rule>\
      </Style>`;
};

module.exports = Styles;
